const express = require("express");
const mongoose = require("mongoose");

const taskRoute = require('./Routes/taskRoute.js');

const app = express();
const port = 3001;

	mongoose.connect("mongodb+srv://admin:admin@batch245-manalang.eyiiu2i.mongodb.net/s35-discussion?retryWrites=true&w=majority",
	{
		//Allows us to avoid any current and future errors while connecting to mongoDB
		useNewUrlParser: true,
		useUnifiedTopology: true
	})

	let db = mongoose.connection;

	//error handling in connecting - icapture yung error
	db.on("error", console.error.bind(console, "Connection error"));

	//this will be triggered if the connection is successful.
	db.once("open", ()=> console.log("We are now connected to the cloud!"))

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// routing

app.use("/tasks", taskRoute)

app.listen(port, () => console.log(`Server is running at port ${port}!`))

/*
	Separation of concerns:
		1. Model should be connected to the controller.
		2. Controller should be connected to the Routes.
		3. Route should be connected to the server/application.
*/